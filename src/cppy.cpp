#define PY_SSIZE_T_CLEAN
#include <Python.h>

#include <iostream>

static PyObject* test_print(PyObject* self, PyObject* args) {
    const char* s;
    if (!PyArg_ParseTuple(args, "s", &s)) return NULL;
    std::cout << "test_print: " << s << std::endl;
    return Py_BuildValue("i", 31337);
}

static PyMethodDef CppyMethods[] = {
    {"test_print",  test_print, METH_VARARGS, "Print to stdout."},
    {NULL, NULL, 0, NULL},
};

static struct PyModuleDef cppy = {
    PyModuleDef_HEAD_INIT,
    "cppy",   /* name of module */
    NULL,     /* module documentation, may be NULL */
    -1,       /* size of per-interpreter state of the module,
                 or -1 if the module keeps state in global variables. */
	CppyMethods
};

PyMODINIT_FUNC PyInit_cppy(void) {
    PyObject *m;
    m = PyModule_Create(&cppy);
    if (m == NULL) return NULL;
    return m;
}
