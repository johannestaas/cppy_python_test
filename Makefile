PY_INCLUDES := $(shell python3.6-config --includes)
PY_LIBS := $(shell python3.6-config --libs)

cppy:
	g++ -shared -o cppy.so -fPIC src/cppy.cpp $(PY_INCLUDES) $(PY_LIBS)

clean:
	rm cppy.so

all: cppy
